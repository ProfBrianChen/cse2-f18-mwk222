//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab03 - CardGenerator
//random number will be generated and based on that number, the card number and suit will be printed

public class CardGenerator{ //class is CardGenerator
  
  public static void main(String[] args){ //main method required for Java programs
    
    int cardNumber = (int)((Math.random()*52)+1); //generates a random integer from 1-52
    
    String cardSuitString = ""; //creates a string for the card suit and allows it to output one of the if statements
      if (cardNumber <= 13) { //if the card is between 1-13, the suit will be diamonds
        cardSuitString = "Diamonds";
      } //this if statement ends
      else if (cardNumber <= 26) { //if the card is between 14-26, the suit will be clubs
        cardSuitString = "Clubs";
      } //this if statement ends
      else if (cardNumber <= 39) { //if the card is between 27-39, the suit will be hearts
        cardSuitString = "Hearts";
      } //this if statement ends
      else if (cardNumber <= 52) { //if the card is between 40-52, the suit will be spades
        cardSuitString = "Spades";
      } //this if statement ends
    
     String cardNumberString = ""; //creates a string for the card numbers and allows it output the number, including facecards
      switch (cardNumber % 13 + 1) { //switches the card number to assign it an identity from the cases below
          //modulus operator is used so that any number will be divided by 13 and the remainder yields card numbers 1-13
          //remainders would yield 0-12, but the adding of 1 allows the cards to be properly ordered 1-13, 13 being a King
        case 1: cardNumberString = "Ace"; //switch to 1 yields an ace
          break;
        case 2: cardNumberString = "2"; //switch to 2 yields a 2
          break;
        case 3: cardNumberString = "3"; //switch to 3 yields a 3
          break;
        case 4: cardNumberString = "4"; //switch to 4 yields a 4
          break;
        case 5: cardNumberString = "5"; //switch to 5 yields a 5
          break;
        case 6: cardNumberString = "6"; //switch to 6 yields a 6
          break;
        case 7: cardNumberString = "7"; //switch to 7 yields a 7
          break;
        case 8: cardNumberString = "8"; //switch to 8 yields a 8
          break;
        case 9: cardNumberString = "9"; //switch to 9 yields a 9
          break;
        case 10: cardNumberString = "10"; //switch to 10 yields a 10
          break;
        case 11: cardNumberString = "Jack"; //switch to 11 yields a Jack
          break;  
        case 12: cardNumberString = "Queen"; //switch to 12 yields a Queen
          break;          
        case 13: cardNumberString = "King"; //switch to 13 yields a King
          break;
    } //switch ends
    
    System.out.print("You picked the " + cardNumberString + " of " + cardSuitString + ".");

  } //end of main method
} //end of class
    