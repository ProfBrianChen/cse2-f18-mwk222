//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab06 - PatternA
//10/15/18

import java.util.Scanner; //imports a scanner to the program

public class PatternA{ //defines the class PatternA
  public static void main(String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); //defines a scanner called myScanner
    
    System.out.println("Enter an integer from 1-10: "); //prompts user to enter an integer from 1-10
    int numRows = 0; //defines integer numRows which the user decides
    
    boolean correctInt = false; //defines the boolean correctInt
    while (!correctInt){ //begins a while loop with the notion that if an integer is not entered, the user must try again
      correctInt = myScanner.hasNextInt(); //coorectInt takes an input
      
      if (correctInt == true){ //when an integer is entered, the scanner will read it
        while (!(numRows >= 1 && numRows <= 10)){ //creates a while loop that runs the interior when the integer isn't 1-10
            numRows = myScanner.nextInt(); //scanner lets user enter the number of rows they want
            System.out.println("Integer must be between 1-10: "); //error displays when a value is not 1-10
        } //end of while loop
      } //end of if statement
      else { //runs when a user does not input an integer
        System.out.println("Invalid input, please enter an integer: "); //if the user does not enter an integer, they are prompted to try again
        myScanner.next(); //removes unwanted input
        } //end of else statement
      
    } //end of while loop
  
    int rowCount = 1; //defines rowCount
    int i = 1; //defines i
    int j = 1; //defines j
    for (i = 1; i <= numRows; i++){ //creates outer for loop that creates a line for each row the user generates
      for (j = 1; j <= rowCount; j++){ //creates inner for loop that adds the next number to the end of the line
        System.out.print(j + " "); //displays number and a space
      } //end of inner loop
      System.out.println(); //returns to next line
      rowCount++; //increases the row count by 1
    } //end of outer loop
 
  } //end of main method
} //end of class


   