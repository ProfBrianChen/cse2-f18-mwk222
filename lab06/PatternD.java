//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab06 - PatternD
//10/15/18

import java.util.Scanner; //imports a scanner to the program

public class PatternD{ //defines the class PatternD
  public static void main(String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); //defines a scanner called myScanner
    
    System.out.println("Enter an integer from 1-10: "); //prompts user to enter an integer from 1-10
    int numRows = 0; //defines integer numRows which the user decides
    
    boolean correctInt = false; //defines the boolean correctInt
    while (!correctInt){ //begins a while loop with the notion that if an integer is not entered, the user must try again
      correctInt = myScanner.hasNextInt(); //coorectInt takes an input
      
      if (correctInt == true){ //when an integer is entered, the scanner will read it
        while (!(numRows >= 1 && numRows <= 10)){ //creates a while loop that runs the interior when the integer isn't 1-10
            numRows = myScanner.nextInt(); //scanner lets user enter the number of rows they want
            System.out.println("Integer must be between 1-10: "); //error displays when a value is not 1-10
        } //end of while loop
      } //end of if statement
      else { //runs when a user does not input an integer
        System.out.println("Invalid input, please enter an integer: "); //if the user does not enter an integer, they are prompted to try again
        myScanner.next(); //removes unwanted input
        } //end of else statement
      
    } //end of while loop
  
    int rowCount = numRows; //defines integer rowCount and makes it equal to numRows
    int i = 1; //defines integer i 
    int j = 1; //defines integer j
    for (i = 1; i <= numRows; i++){ //creates outer for loop that begins at 1 and increments up until i is greater than numRows  
      for (j = rowCount; j >= 1; j--){ //creates inner for loop that begins with the number of rows generated, and increments down to 1
        System.out.print(j + " "); //displays number and a space
      } //end of inner for loop
      System.out.println(); //returns to the next line
      rowCount--; //decreases the length of the next row by 1
    } //end of outer for loop
 
  } //ends main method
} //ends class


   