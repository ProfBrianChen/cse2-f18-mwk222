//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw05 
//program will generate a certain number of hands and determine the number of hands with four of a kind, three of a kind, two pair, and one pair

import java.util.Scanner; //imports the scanner

public class Hw05{ //defines the class Hw05
  public static void main(String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); //defines a new scanner called myScanner
    //defines integers for pair counts
    int numQuadsCount = 0; 
    int numTripsCount = 0;
    int numTwoPairCount = 0;
    int numOnePairCount = 0;
    //defines integers for cards
    int card1 = 0; 
    int card2 = 0; 
    int card3 = 0; 
    int card4 = 0; 
    int card5 = 0;
    
    System.out.println("How many hands would you like to generate?"); //prompts user to enter how many hands they would like to generate
   
    boolean correctInt = false; //defines a boolean for correct integers
    int numHands = 10; //defines number of hands and initializes it
    while(!correctInt){ //begins while loop to receive an integer from user
      correctInt = myScanner.hasNextInt(); //allows user to input an integer
      if (correctInt == true){
        numHands = myScanner.nextInt(); //integer allows scanner to move on
        }
      else {
        System.out.println("Invalid input, please enter an integer:"); //if the user does not enter an integer, an error statement is shown to the user
        myScanner.next(); //skips the input the user entered and back to enter an integer
        }
      } //ends while loop
    
    int a = 0; //defines a for the for loop
    //for loop that randomizes 5 cards, makes sure they are distinct, and counts the number of one pair, two pair, three of a kind, and four of a kind
    for (a = 1; a <= numHands; a++){ //for loop that runs however many hands the user wants to be generated
      //randomizes all 5 cards from 1-52
      card1 = (int)((Math.random()*52)+1);
      card2 = (int)((Math.random()*52)+1);
      card3 = (int)((Math.random()*52)+1);
      card4 = (int)((Math.random()*52)+1);
      card5 = (int)((Math.random()*52)+1);
      
      //four while loops that randomizes rest of the cards to ensure all are distinct
      while (card1 == card2){
        card2 = (int)((Math.random()*52)+1);
      }
      while (card2 == card3){
        card3 = (int)((Math.random()*52)+1);
      }
      while (card3 == card4){
        card4 = (int)((Math.random()*52)+1);
      }
      while (card4 == card5){
        card5 = (int)((Math.random()*52)+1);
      }
    
      //divides all the card values by 13 and takes the remainder, so the cards are 1-13
      card1 = card1 % 13+1;
      card2 = card2 % 13+1;
      card3 = card3 % 13+1;
      card4 = card4 % 13+1;
      card5 = card5 % 13+1;
 
      //one pair statments
      //defines all the scenarios in which two cards are the same and the other three are different
      //if the cards are the same, the one pair counter is incremented by one
      if(card1 == card2 && card1 != card3 && card1 != card4 && card1 != card5){
        numOnePairCount++;
      }
      else if(card1 == card3 && card1 != card2 && card1 != card4 && card1 != card5){
        numOnePairCount++;
      }
      else if(card1 == card4 && card1 != card2 && card1 != card3 && card1 != card5){
        numOnePairCount++; 
      }
      else if(card1 == card5 && card1 != card2 && card1 != card3 && card1 != card4){
        numOnePairCount++;  
      }
      else if(card2 == card3 && card2 != card1 && card2 != card4 && card2 != card5){
        numOnePairCount++; 
      }
      else if(card2 == card4 && card2 != card1 && card2 != card3 && card2 != card5){
        numOnePairCount++;
      }
      else if(card2 == card5 && card2 != card1 && card2 != card3 && card2 != card4){
        numOnePairCount++;
      }
      else if(card3 == card4 && card3 != card1 && card3 != card2 && card3 != card5){
        numOnePairCount++;
      }
      else if(card3 == card5 && card3 != card1 && card3 != card2 && card3 != card4){
        numOnePairCount++;
      }
      else if(card4 == card5 && card4 != card1 && card4 != card2 && card4 != card3){
        numOnePairCount++;
      }
      
      //two pair statements
      //defines all the scenarios in which there are two pairs and the last card is different
      //if there is a two pair, the two pair counter is incremented by one
      if (card1 == card2 && card3 == card4 && card1 != card3 && card1 != card5){
        numTwoPairCount++;
      }
      else if (card1 == card2 && card4 == card5 && card1 != card4 && card1 != card3){
        numTwoPairCount++;
      }
      else if (card1 == card2 && card3 == card5 && card1 != card3 && card1 != card4){
        numTwoPairCount++;
      }
      else if (card1 == card3 && card2 == card4 && card1 != card2 && card1 != card5){
        numTwoPairCount++;
      }
      else if (card1 == card3 && card4 == card5 && card1 != card4 && card1 != card2){
        numTwoPairCount++;
      }
      else if (card1 == card3 && card2 == card5 && card1 != card2 && card1 != card4){
        numTwoPairCount++;
      }
      else if (card1 == card4 && card2 == card3 && card1 != card2 && card1 != card5){
        numTwoPairCount++;
      }
      else if (card1 == card4 && card2 == card5 && card1 != card2 && card1 != card3){
        numTwoPairCount++;
      }
      else if (card1 == card4 && card3 == card5 && card1 != card3 && card1 != card2){
        numTwoPairCount++;
      }
      else if (card1 == card5 && card2 == card3 && card1 != card2 && card1 != card4){
        numTwoPairCount++;
      }
      else if (card1 == card5 && card2 == card4 && card1 != card2 && card1 != card3){
        numTwoPairCount++;
      }
      else if (card1 == card5 && card3 == card4 && card1 != card3 && card1 != card2){
        numTwoPairCount++;
      }
      else if (card2 == card3 && card4 == card5 && card2 != card4 && card2 != card1){
        numTwoPairCount++;
      }
      else if (card2 == card4 && card3 == card5 && card2 != card3 && card2 != card1){
        numTwoPairCount++;
      }
      else if (card2 == card5 && card3 == card4 && card2 != card3 && card2 != card1){
        numTwoPairCount++;
      }
      
      //three of a kind statements
      //defines all the scenarios in which there are three cards of the same value and the last two are different
      //if there is a three of a kind, the trips counter is incremented by one
      if (card1 == card2 && card1 == card3 && card1 != card4 && card1 != card5){
        numTripsCount++;
      }
      else if (card1 == card2 && card1 == card4 && card1 != card3 && card1 != card5){
        numTripsCount++;
      }
      else if (card1 == card2 && card1 == card5 && card1 != card3 && card1 != card4){
        numTripsCount++;
      }
      else if (card1 == card3 && card1 == card4 && card1 != card2 && card2 != card5){
        numTripsCount++;
      }
      else if (card1 == card3 && card1 == card5 && card1 != card2 && card1 != card4){
        numTripsCount++;
      }
      else if (card1 == card4 && card1 == card5 && card1 != card2 && card1 != card3){
        numTripsCount++;
      }
      else if (card2 == card3 && card2 == card4 && card2 != card1 && card2 != card5){
        numTripsCount++;
      }
      else if (card2 == card3 && card2 == card5 && card2 != card1 && card2 != card4){
        numTripsCount++;
      }
      else if (card2 == card4 && card2 == card5 && card2 != card1 && card2 != card3){
        numTripsCount++;
      }
      else if (card3 == card4 && card3 == card5 && card3 != card1 && card3 != card2){
        numTripsCount++;
      }
      
      //four of a kind statements
      //defines all the scenarios in which there is a four of a kind and the last card is different
      //if there is a four of a kind, the quads counter is incremented by one
      if (card1 == card2 && card1 == card3 && card1 == card4 && card1 != card5){
        numQuadsCount++;
      }
      else if (card1 == card2 && card1 == card3 && card1 == card5 && card1 != card4){
        numQuadsCount++;
      }
      else if (card1 == card2 && card1 == card4 && card1 == card5 && card1 != card3){
        numQuadsCount++;
      }
      else if (card1 == card3 && card1 == card4 && card1 == card5 && card1 != card2){
        numQuadsCount++;
      }
      else if (card2 == card3 && card2 == card4 && card2 == card5 && card2 != card1){
        numQuadsCount++;
      }
        
    } //ends the for loop
    
    
    System.out.println("The number of loops: " + numHands); //displays the number of loops the user wants generated
    System.out.printf("The probability of four-of-a-kind: %.3f\n", (numQuadsCount/(double)numHands)); //displays the probability of a four of a kind
    System.out.printf("The probability of three-of-a-kind: %.3f\n", (numTripsCount/(double)numHands)); //displays the probability of a three of a kind
    System.out.printf("The probability of two-pair: %.3f\n", (numTwoPairCount/(double)numHands)); //displays the probability of a two pair
    System.out.printf("The probability of one-pair: %.3f\n", (numOnePairCount/(double)numHands)); //displays the porbability of a one pair
 
  } //ends main method
} //ends class
