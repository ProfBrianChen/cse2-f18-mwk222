//Matt Knowles
//mwk222@lehigh.edu
//CSE 002-110 
//hw02 - Arithmetic Calculations

public class Arithmetic{
  
  public static void main(String[] args){
    //stored variables will be manipulated to produce calculations for store item costs
    
    int numPants = 3; //Number of pairs of pants
    double pantPrice = 34.98; //Cost of one pair of pants
    int numShirts = 2; //Number of shirts
    double shirtPrice = 24.99; //Cost of one shirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //Cost of one belt
    double paSalesTax = 0.06; //Tax rate
    
    double totalCostOfPants; //Total cost of pants
    double totalCostOfShirts; //Total cost of shirts
    double totalCostOfBelts; //Total cost of belts
    
    double salesTaxOfPants; //Tax charged on pants
    double salesTaxOfShirts; //Tax charged on shirts
    double salesTaxOfBelts; //Tax charged on belts
    
    double totalCostBeforeTax; //Total cost of items before tax
    double totalSalesTax; //Total sales tax
    double totalCostAfterTax; //Total cost of items after tax
    
    totalCostOfPants = numPants * pantPrice; //Cost of pants is the number of pants times price of pants
    totalCostOfShirts = numShirts * shirtPrice; //Cost of shirts is the number of shirts times price of shirts
    totalCostOfBelts = numBelts * beltPrice; //Cost of belts is the number of belts times price of belts
    
    salesTaxOfPants = totalCostOfPants * paSalesTax; //Sales tax of pants is the total cost of pants times the sales tax
    salesTaxOfShirts = totalCostOfShirts * paSalesTax; //Sales tax of shirts is the total cost of shirts times the sales tax
    salesTaxOfBelts = totalCostOfBelts * paSalesTax; //Sales tax of belts is the total cost of belts times the sales tax
    
    totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //Cost of all items before tax is the sum of the total costs
    totalSalesTax = salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts; //Total sales tax is the sum of the sales tax on all the items
    totalCostAfterTax = totalCostBeforeTax + totalSalesTax; //Cost of all items after tax is the sum of total cost before tax and total sales tax
    
    salesTaxOfPants = salesTaxOfPants * 100; //takes a value like 2.6453645 and brings the only 3 values needed to the left of the decimal
    int middleStepOfPants = (int)salesTaxOfPants; //turns value into an integer by chopping the decimal off so that 264 is left
    double roundedTaxOfPants = middleStepOfPants / 100.0; //turns value back to a double and divides by 100.0 to make value rounded to two decimal places, as money is
    
    salesTaxOfShirts = salesTaxOfShirts * 100; //same procedure as 42-44
    int middleStepOfShirts = (int)salesTaxOfShirts;
    double roundedTaxOfShirts = middleStepOfShirts / 100.0;
    
    salesTaxOfBelts = salesTaxOfBelts * 100; //same procedure as 42-44 
    int middleStepOfBelts = (int)salesTaxOfBelts;
    double roundedTaxOfBelts = middleStepOfBelts / 100.0;
    
    totalSalesTax = totalSalesTax * 100; //same procedure as 42-44
    int middleStepOfSalesTax = (int)totalSalesTax;
    double roundedSalesTax = middleStepOfSalesTax / 100.0;
    
    totalCostAfterTax = totalCostAfterTax * 100; //same procedure as 42-44
    int middleStepOfCost = (int)totalCostAfterTax;
    double roundedCostAfterTax = middleStepOfCost / 100.0;
    
    System.out.println("Total cost of pants: $" + totalCostOfPants); //displays cost of pants (before tax)
    System.out.println("Total cost of shirts: $" + totalCostOfShirts); //displays cost of shirts (before tax)
    System.out.println("Total cost of belts: $" + totalCostOfBelts); //displays cost of belts (before tax)
    
    System.out.println("Sales tax of pants: $" + roundedTaxOfPants); //displays sales tax of pants
    System.out.println("Sales tax of shirts: $" + roundedTaxOfShirts); //displays sales tax of shirts
    System.out.println("Sales tax of belts: $" + roundedTaxOfBelts); //displays sales tax of belts
    
    System.out.println("Total cost of items before tax: $" + totalCostBeforeTax); //displays total cost (before tax)
    System.out.println("Total sales tax: $" + roundedSalesTax); //displays total sales tax
    System.out.println("Total cost of items after tax: $" + roundedCostAfterTax); //displays total cost (after tax)
    
  } //end of main
} //end of class