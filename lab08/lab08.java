//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab08
//11/9/18

import java.util.Arrays; //imports the array method
import java.lang.Math; //imports the math method

public class lab08{ //class is lab08
  public static void main(String[] args){
    
    int[] myList = new int[100]; //declares, defines, and sets a new array for the list of integers to have an index of 100
    int[] count = new int[100]; //declares, defines, and sets a new array for the count to have an index of 100
    
    for (int i = 0; i < myList.length; i++){ //for loop to randomize the first list 
      myList[i] = (int)(Math.random() * 100); //assigns each index of the list a random integer from 0-99
    }
    
    System.out.println(Arrays.toString(myList)); //displays the list
    
    for (int i = 0; i < myList.length; i++){ //for loop to count the occurences 
      int a = myList[i]; //assigns int a to the list
      count[a]++; //count is incremented
    }
    
    for (int i = 0; i < count.length; i++){ //for loop to print the occurences
      if(count[i] == 1){
        System.out.println(i + " occurs " + count[i] + " time"); //when it occurs once, it will say "time" instead of "times"
      }
      else if(count[i] > 1){
        System.out.println(i + " occurs " + count[i] + " times"); //all other counts will say "times"
      }
    }
      
      
    
  } //end of main method
} //end of public class
