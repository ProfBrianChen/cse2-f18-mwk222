// CSE 002 Welcome Class Assignment
public class WelcomeClass{
  
  public static void main(String args[]){
    //Welcome message with signature will be displayed on the terminal
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--W--K--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("Hello, my name is Matthew and I'm from the constant-temperature state of California.");
    System.out.println("I'm really into playing and watching soccer, and I am very excited for this semester of learning computer science.");
  }
}