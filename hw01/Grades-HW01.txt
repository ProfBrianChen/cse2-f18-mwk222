Homework 1 Grading Rubric

75%: The code compiles
    yay!
    
20%: The code prints the proper symbols
    -1%: Forgot to print the last line of 'v'
    
5%: Style
    -2%: Make sure to include your name and email in a header comment for the next program.
    
Lateness:
    Note: no penalty for the first homework
    
Final Grade: 97%