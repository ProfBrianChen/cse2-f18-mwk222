//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw08 - shuffle
//11/13/18

import java.util.*; //imports all the methods needed: scanner, arrays, and math

public class hw08{ //class is hw08
  public static void main(String[] args) { //main method
  
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames = {"C","H","S","D"}; //creates suitNames array with the 4 suits
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //creates the rankNames array with all the numbers in a deck
    String[] cards = new String[52]; //creates cards array with index of 52, size of a deck
    String[] hand = new String[5]; //creates hand array with index of 5
    int numCards = 5; //declares and initializes numCards
    int again = 1; //declares and initializes again
    int index = 51; //declares and initializes index
    
    //for loop to turn i into a real card number with suit
    for (int i = 0; i < 52; i++){ 
      cards[i] = rankNames[i % 13] + suitNames[i / 13]; 
    }
    
    printArray(cards); //calls the printArray method to print the list of cards
    shuffle(cards); //shuffles the original deck of cards
    printArray(cards); //prints out shuffled deck of cards
    
    //while loop to print the hand with user-chosen size
    while(again == 1){ 
      hand = getHand(cards, index, numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); //asks user if they want another hand
      again = scan.nextInt(); //allows user to enter 1 for new hand and anything else to quit
      }
      
    } //end of main
    
  public static void printArray(String[] list){ //printArray method
    //for loop that prints a list of 52 cards
    for (int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
    
  } //end of printArray
  
  public static void shuffle(String[] list){ //shuffle method
    System.out.println("Shuffled"); //tells user the next list of cards is shuffled
    
    int j = 0;
    //for loop that randomizes j and swaps list[j] with list[0] and loops 200 times to properly shuffle the deck
    for (int i = 0; i < 200; i++){
      j = (int)((Math.random() * 51) + 1);
      String temp = list[0];
      list[0] = list[j];
      list[j] = temp;
      j++;
    } 
    
  } //end of shuffle
  
  public static String[] getHand(String[] list, int index, int numCards){ //getHand method
    Scanner myScanner = new Scanner(System.in); //declares a new scanner
    System.out.println("Enter how many cards you would like in your hand: "); //prompts user to choose size of hand
    numCards = myScanner.nextInt(); //allows for user input
    
    System.out.println("Hand"); 
    String[] newList = new String[numCards]; //creates a new array the size of the hand the user wants
    
    //for loop to take the last numCards the user wants to make a hand with
    for (int i = 0; i < numCards; i++){
      newList[i] = list[index - i];
    }
    
    return newList; //returns the new list (hand)
  } //end of getHand
  
} //end of class
    





