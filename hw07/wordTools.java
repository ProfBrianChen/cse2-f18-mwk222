//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw07 - word tools
//10/28/18

import java.util.Scanner; //imports the Scanner

public class wordTools { //creates class wordTools
  public static void main(String[] args){ //main method
    
    Scanner myScanner = new Scanner(System.in); //creates a scanner called myScanner
    
    System.out.println("Enter a sample text:"); //prompts user to enter a sentence
    String userText = sampleText(myScanner); //calls the sampleText method
    System.out.println("You entered: " + userText); //shows user what they entered

    char choice = printMenu(myScanner); //shows the menu of options
    
    
    int userTextWhiteSpaces = getNumOfNonWSCharacters(userText); //creates an integer that counts non-whitespace characters
    int userTextWords = getNumOfWords(userText); //creates an integer that counts number of words
    
    String enterPeriods = replaceExclamation("Edited text: " + userText); //creates string that replaces exclamation marks with periods
    String shortSpaces = shortenSpace("Edited text: " + userText); //creates string that replaces multi-whitespace sections with one space
    
    //when user enters one of the characters from the menu it displays its function
    if (choice == 'q'){
      System.out.println("Exited"); 
      return;
    }
    else if (choice == 'c'){
      System.out.println("Number of non-whitespace characters: " + userTextWhiteSpaces); 
      return;
    }
    else if (choice == 'w'){
      System.out.println("Number of words: " + userTextWords);
      return;
    }
    else if (choice == 'r'){
     System.out.println(enterPeriods);
      return;
    }
    else if (choice == 's'){
      System.out.println(shortSpaces);
      return;
    }
    String textFound = "";
    int numUserTextFound = findText(textFound, userText);
    if (choice == 'f'){
      System.out.println("Number of instances: " + numUserTextFound);
      return;
    }
    
  }
  
    public static String sampleText(Scanner myScanner){ //sampleText method
      String text = "";
      
      if (myScanner.hasNext()) {
        text = myScanner.nextLine(); //allows user to enter a sentence
      }
      
      return text; //returns the text
    }
    
    public static char printMenu(Scanner myScanner){ //printMenu method
      //defines and initializes all the menu options
      char q = 'q';
      char c = 'c';
      char w = 'w';
      char f = 'f';
      char r = 'r';
      char s = 's';
      
      //displays the options
      System.out.println("MENU");
      System.out.println(c + " - Number of non-whitespace characters");
      System.out.println(w + " - Number of words");
      System.out.println(f + " - Find text");
      System.out.println(r + " - Replace all !'s");
      System.out.println(s + " - Shorten spaces");
      System.out.println(q + " - Quit");
      System.out.println("Choose an option:");
      
      char option = myScanner.next(".").charAt(0); //allows an option to be chosen
      
      return option; //returns the option chosen
    }
    
    public static int getNumOfNonWSCharacters(String userText){ //getNumOfNonWSCharacters method
      int whiteSpaces = 0;
      for (int i = 0; i < userText.length(); i++){ //moves from one character to the next
        if(userText.charAt(i) == ' '){ //counts number of whitespaces
          whiteSpaces++;
        }
      }
      
      return userText.length() - whiteSpaces; //returns the length of the userText and takes out spaces
    }
    
    public static int getNumOfWords(String userText){ //getNumOfWords method
      int count = 0;
      for (int i = 0; i < userText.length(); i++){ //moves from one character to the next
        if (userText.charAt(i) != ' '){
          count++; //when a character moves and the next character is not a space, count increments
          while(userText.charAt(i) != ' ' && i < userText.length() - 1){
            i++;
          }
        }
      }
      return count; //returns the count
    }
    
    public static int findText(String textFound, String userText){ //findText method
      Scanner myScanner = new Scanner(System.in);
      System.out.println("Enter a word or phrase to be found:"); //prompts user to enter something to be found
      textFound = myScanner.nextLine();
      int count = 0;
      int i = 0;
      while (i != -1){ 
        i = userText.indexOf(textFound, i); //i goes through the text to find substring user enters
        if (i != -1){
          count++; //increments the count
          i += textFound.length();
        }
      }
  
      return count; //returns the count
    }
    
    public static String replaceExclamation(String userText){ //replaceExclamation method
      String newString = "";
      newString = userText.replace('!', '.'); //newString equals the userText with a replacement of a period for exclamation marks
       
      return newString; //returns the new string
    }
    
    public static String shortenSpace(String userText){ //shortenSpace method
      String newString = "";
      newString = userText.replaceAll("( )+", " "); //takes long spaces and replaces them with a single space
      
      return newString; //returns the new space
    }
}