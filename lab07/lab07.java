//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab07 - story generation
//10/26/18

import java.util.Random; //imports random
import java.util.Scanner; //imports scanner

public class lab07{ //class lab07
  public static void main(String[] args){ //main method
    Scanner scnr = new Scanner(System.in);
    Random randomGen = new Random();
    int i = 1;
    
    String adjective = "";
    adjective = randomAdj(adjective); //adjective calls the random adjective method
    
    String subject = "";
    subject = randomSub(subject); //subject calls the random subject method
    
    String verb = "";
    verb = randomVerb(verb); //verb calls the random verb method
    
    String object = "";
    object = randomObj(object); //object calls the random object method
    
    System.out.print("The " + adjective + " " + subject + " " + verb + " the "); //prints first statement
    adjective = randomAdj(adjective); //switches adjective to new random one
    System.out.println(adjective + " " + object + "."); //prints rest of statement
    
    while (i != 0){ //while loop that quits if 0 is entered
      System.out.println("Enter 1 for a new sentence, enter 0 to quit: ");
      i = scnr.nextInt();
      if (i == 1){ //if they type 1, it generates a new sentence
        adjective = randomAdj(adjective); //randomizes all the words going into the sentence
        subject = randomSub(subject);
        verb = randomVerb(verb);
        object = randomObj(object);
        
        System.out.print("The " + adjective + " " + subject + " " + verb + " the "); //prints a new statement with a random second adjective
        adjective = randomAdj(adjective);
        System.out.println(adjective + " " + object + ".");
      }
      else if (i == 0){ //loop quits with 0
        break;
      }
      else { //anything else entered will prompt the user to try again
        System.out.println("Please enter either 1 for a new sentence or 0 to quit");
        i = scnr.nextInt();
        
      }
    }
    
    String actionVerb = "";
    actionVerb = thesis(actionVerb); //actionVerb calls the random action verb method for thesis
    
    System.out.println("This " + subject + " was " + actionVerb + " the " + adjective + " " + object + "."); //thesis
    
    System.out.println("That " + subject + " " + verb + " its " + object + "!"); //conclusion
    
          
  }
    public static String randomAdj(String adjString){ //method creates random adjective
      Random randomInt = new Random();
      int num = randomInt.nextInt(10); //gets a random int from 0-9
      
      switch (num){ //switches the random integer to an adjective
        case 0: adjString = "tall";
          break;
        case 1: adjString = "short";
          break;
        case 2: adjString = "young";
          break;
        case 3: adjString = "old";
          break;
        case 4: adjString = "strong";
          break;
        case 5: adjString = "weak";
          break;
        case 6: adjString = "fantastic";
          break;
        case 7: adjString = "terrible";
          break;
        case 8: adjString = "cool";
          break;
        case 9: adjString = "boring";
          break;
      }
      return adjString; //returns the adjective
    }
    
    public static String randomSub(String subString){ //method creates random subject
      Random randomInt = new Random();
      int num = randomInt.nextInt(10); //gets a random int from 0-9
      
      switch (num){ //switches the random integer to a subject
        case 0: subString = "man";
          break;
        case 1: subString = "woman";
          break;
        case 2: subString = "child";
          break;
        case 3: subString = "tree";
          break;
        case 4: subString = "squirrel";
          break;
        case 5: subString = "cat";
          break;
        case 6: subString = "butterfly";
          break;
        case 7: subString = "apple";
          break;
        case 8: subString = "banana";
          break;
        case 9: subString = "unicorn";
          break;
      }
      return subString; //returns the subject
    }
    
    public static String randomVerb(String verbString){ //method creates random verb
      Random randomInt = new Random();
      int num = randomInt.nextInt(10); //gets a random int from 0-9
      
      switch (num){ //switches the random integer to a verb
        case 0: verbString = "ran";
          break;
        case 1: verbString = "jumped";
          break;
        case 2: verbString = "swam";
          break;
        case 3: verbString = "ate";
          break;
        case 4: verbString = "drank";
          break;
        case 5: verbString = "looked";
          break;
        case 6: verbString = "danced";
          break;
        case 7: verbString = "played";
          break;
        case 8: verbString = "threw";
          break;
        case 9: verbString = "yelled";
          break;
      }
      return verbString; //returns the verb
    }
    
    public static String randomObj(String objString){ //method creates random object
      Random randomInt = new Random();
      int num = randomInt.nextInt(10); //gets a random int from 0-9
      
      switch (num){ //switches the random integer to an object
        case 0: objString = "sweater";
          break;
        case 1: objString = "present";
          break;
        case 2: objString = "river";
          break;
        case 3: objString = "basketball";
          break;
        case 4: objString = "soccer ball";
          break;
        case 5: objString = "TV";
          break;
        case 6: objString = "blanket";
          break;
        case 7: objString = "fan";
          break;
        case 8: objString = "rug";
          break;
        case 9: objString = "phone";
          break;
      }
      return objString; //returns the object
    }
    
    public static String thesis(String actionVerb){ //method creates verb for thesis
    
      actionVerb = "";
      Random randomInt = new Random();
      int num = randomInt.nextInt(10); //gets a random int from 0-9
      
      switch (num){ //switches the random integer to an action verb
        case 0: actionVerb = "watching";
          break;
        case 1: actionVerb = "walking";
          break;
        case 2: actionVerb = "eating";
          break;
        case 3: actionVerb = "flying";
          break;
        case 4: actionVerb = "learning";
          break;
        case 5: actionVerb = "thinking";
          break;
        case 6: actionVerb = "running";
          break;
        case 7: actionVerb = "listening";
          break;
        case 8: actionVerb = "playing";
          break;
        case 9: actionVerb = "dancing";
          break;
      }
      
      return actionVerb; //returns the action verb
    }
    
    public static String conclusion(String conclusion){ //method creates conclusion
      conclusion = "";
      String adjective = "";
      adjective = randomAdj(adjective);
    
      String subject = "";
      subject = randomSub(subject);
    
      String verb = "";
      verb = randomVerb(verb);
    
      String object = "";
      object = randomObj(object);
      
      System.out.println("That " + subject + " " + verb + " its " + object + "!");
      
      return conclusion;
    }
}