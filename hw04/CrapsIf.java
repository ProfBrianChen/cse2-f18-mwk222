//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab03 - CrapsIf
//program will ask the user for either a random selection of two die numbers or them to choose two
//program will then display the slang language of the two die in Craps

import java.util.Scanner; //this allows the Scanner class to be used without getting error messages

public class CrapsIf{ //class is CrapsIf
  
  public static void main(String[] args){ //main method required for Java programs
 
    Scanner myScanner = new Scanner(System.in); //generates a new scanner to allow user input
    System.out.println("Would you like to get a random cast or choose your own two dice numbers?"); 
    System.out.println("Enter (1) for random or (2) to choose: "); //user is prompted to choose a random roll or choosing their roll
    int userChoice = myScanner.nextInt(); //allows user to input random roll or choice roll
    
    int diceNumber1 = 1; //initializes diceNumber1
    int diceNumber2 = 2; //initializes diceNumber2
    
    String userChoiceString = ""; //creates string for user to choose 1 or 2, random or choice
      if (userChoice == 1){ //if statement for choice 1
          diceNumber1 = (int)((Math.random()*6)+1);
          diceNumber2 = (int)((Math.random()*6)+1); //lines 23 and 24 generates two random integers 1-6
          System.out.println("You rolled: ");
          System.out.println(diceNumber1 + " " + diceNumber2); //displays what random roll generates
          System.out.println("Roll name: "); //displays roll name depending on roll
        
          if (diceNumber1 == 1 && diceNumber2 == 1){ 
              System.out.print("Snake Eyes"); //if their roll is 1 AND 1, it displays "Snake Eyes"
          } //ends statement
          else if ((diceNumber1 == 1 && diceNumber2 == 2) || 
                  (diceNumber1 == 2 && diceNumber2 == 1)){ 
            System.out.print("Ace Deuce"); //if their roll is (1 AND 2) OR (2 AND 1), it displays "Ace Deuce"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 3) || 
                  (diceNumber1 == 3 && diceNumber2 == 1)){
            System.out.print("Easy Four"); //if their roll is (1 AND 3) OR (3 AND 1), it displays "Easy Four"
         }
          else if ((diceNumber1 == 2 && diceNumber2 == 2)){
            System.out.print("Hard Four"); //if their roll is 2 AND 2, it displays "Hard Four"
         }
          else if ((diceNumber1 == 1 && diceNumber2 == 4) || 
                  (diceNumber1 == 4 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 3) ||
                  (diceNumber1 == 3 && diceNumber2 == 2)){
            System.out.print("Fever Five"); //if their roll is (1 AND 4) OR (4 AND 1) OR (2 AND 3) OR (3 AND 2), it displays "Fever Five"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 5) || 
                  (diceNumber1 == 5 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 2)){
            System.out.print("Easy Six"); //if their roll is (1 AND 5) OR (5 AND 1) OR (2 AND 4) OR (4 AND 2), it displays "Easy Six"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 3)){
            System.out.print("Hard Six"); //if their roll is 3 AND 3, it displays "Hard Six"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 5) ||
                  (diceNumber1 == 5 && diceNumber2 == 2) ||
                  (diceNumber1 == 3 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 3)){
            System.out.print("Seven Out"); //if their roll is (1 AND 6) OR (6 AND 1) OR (2 AND 5) OR (5 AND 2) OR (3 AND 4) OR (4 AND 3), it displays "Fever Five"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 5) || 
                  (diceNumber1 == 5 && diceNumber2 == 3) ||
                  (diceNumber1 == 2 && diceNumber2 == 6) ||
                  (diceNumber1 == 6 && diceNumber2 == 2)){
            System.out.print("Easy Eight"); //if their roll is (3 AND 5) OR (5 AND 3) OR (2 AND 6) OR (6 AND 2), it displays "Easy Eight"
          }
          else if ((diceNumber1 == 4 && diceNumber2 == 4)){
            System.out.print("Hard Eight"); //if their roll is 4 AND 4, it displays "Hard Eight"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 3) ||
                  (diceNumber1 == 5 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 5)){
            System.out.print("Nine"); //if their roll is (3 AND 6) OR (6 AND 3) OR (5 AND 4) OR (4 AND 5), it displays "Nine"
          }
          else if ((diceNumber1 == 4 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 4)){
            System.out.print("Easy Ten"); //if their roll is (4 AND 6) OR (6 AND 4), it displays "Easy Ten"
          }
          else if ((diceNumber1 == 5 && diceNumber2 == 5)){
            System.out.print("Hard Ten"); //if their roll is 5 AND 5, it displays "Hard Ten"
          }
          else if ((diceNumber1 == 5 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 5)){
            System.out.print("Yo-Leven"); //if their roll is (5 AND 6) OR (6 AND 5), it displays "Yo-Leven"
          }
          else if ((diceNumber1 == 6 && diceNumber2 == 6)){
            System.out.print("Boxcars"); //if their roll is 6 AND 6, it displays "Boxcars"
          }
                 
      } //ends first if statement
      else if (userChoice == 2){ //other option is the user enters 2 and they get to choose the integers
        System.out.print("Choose first integer from 1-6: "); //prompts user to choose the first integer
        diceNumber1 = myScanner.nextInt(); //allows user input for first integer
        System.out.print("Choose second integer from 1-6: "); //prompts user to choose the second integer
        diceNumber2 = myScanner.nextInt(); //allows user input for second integer
        System.out.println("Roll name: "); //displays roll name for following cases
         
        if (diceNumber1 == 1 && diceNumber2 == 1){ 
              System.out.print("Snake Eyes"); //if their roll is 1 AND 1, it displays "Snake Eyes"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 2) || 
                  (diceNumber1 == 2 && diceNumber2 == 1)){ 
            System.out.print("Ace Deuce"); //if their roll is (1 AND 2) OR (2 AND 1), it displays "Ace Deuce"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 3) || 
                  (diceNumber1 == 3 && diceNumber2 == 1)){
            System.out.print("Easy Four"); //if their roll is (1 AND 3) OR (3 AND 1), it displays "Easy Four"
         }
          else if ((diceNumber1 == 2 && diceNumber2 == 2)){
            System.out.print("Hard Four"); //if their roll is 2 AND 2, it displays "Hard Four"
         }
          else if ((diceNumber1 == 1 && diceNumber2 == 4) || 
                  (diceNumber1 == 4 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 3) ||
                  (diceNumber1 == 3 && diceNumber2 == 2)){
            System.out.print("Fever Five"); //if their roll is (1 AND 4) OR (4 AND 1) OR (2 AND 3) OR (3 AND 2), it displays "Fever Five"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 5) || 
                  (diceNumber1 == 5 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 2)){
            System.out.print("Easy Six"); //if their roll is (1 AND 5) OR (5 AND 1) OR (2 AND 4) OR (4 AND 2), it displays "Easy Six"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 3)){
            System.out.print("Hard Six"); //if their roll is 3 AND 3, it displays "Hard Six"
          }
          else if ((diceNumber1 == 1 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 1) ||
                  (diceNumber1 == 2 && diceNumber2 == 5) ||
                  (diceNumber1 == 5 && diceNumber2 == 2) ||
                  (diceNumber1 == 3 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 3)){
            System.out.print("Seven Out"); //if their roll is (1 AND 6) OR (6 AND 1) OR (2 AND 5) OR (5 AND 2) OR (3 AND 4) OR (4 AND 3), it displays "Fever Five"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 5) || 
                  (diceNumber1 == 5 && diceNumber2 == 3) ||
                  (diceNumber1 == 2 && diceNumber2 == 6) ||
                  (diceNumber1 == 6 && diceNumber2 == 2)){
            System.out.print("Easy Eight"); //if their roll is (3 AND 5) OR (5 AND 3) OR (2 AND 6) OR (6 AND 2), it displays "Easy Eight"
          }
          else if ((diceNumber1 == 4 && diceNumber2 == 4)){
            System.out.print("Hard Eight"); //if their roll is 4 AND 4, it displays "Hard Eight"
          }
          else if ((diceNumber1 == 3 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 3) ||
                  (diceNumber1 == 5 && diceNumber2 == 4) ||
                  (diceNumber1 == 4 && diceNumber2 == 5)){
            System.out.print("Nine"); //if their roll is (3 AND 6) OR (6 AND 3) OR (5 AND 4) OR (4 AND 5), it displays "Nine"
          }
          else if ((diceNumber1 == 4 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 4)){
            System.out.print("Easy Ten"); //if their roll is (4 AND 6) OR (6 AND 4), it displays "Easy Ten"
          }
          else if ((diceNumber1 == 5 && diceNumber2 == 5)){
            System.out.print("Hard Ten"); //if their roll is 5 AND 5, it displays "Hard Ten"
          }
          else if ((diceNumber1 == 5 && diceNumber2 == 6) || 
                  (diceNumber1 == 6 && diceNumber2 == 5)){
            System.out.print("Yo-Leven"); //if their roll is (5 AND 6) OR (6 AND 5), it displays "Yo-Leven"
          }
          else if ((diceNumber1 == 6 && diceNumber2 == 6)){
            System.out.print("Boxcars"); //if their roll is 6 AND 6, it displays "Boxcars"
          } 
      
      } //ends else if statement
 
  } //ends main method
} //ends class