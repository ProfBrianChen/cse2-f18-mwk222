//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab03 - CrapsSwitch
//program will ask the user for either a random selection of two die numbers or them to choose two
//program will then display the slang language of the two die in Craps

import java.util.Scanner; //this allows the Scanner class to be used without getting error messages

public class CrapsSwitch{ //class is CrapsSwitch
  
  public static void main(String[] args){ //main method required for Java programs
 
    Scanner myScanner = new Scanner(System.in); //generates a new scanner to allow user input
    System.out.println("Would you like to get a random cast or choose your own two dice numbers?"); 
    System.out.println("Enter (1) for random or (2) to choose: "); //user is prompted to choose a random roll or choosing their roll
    int userChoice = myScanner.nextInt(); //allows user to input random roll or choice roll
    
    int diceNumber1 = 1; //initializes diceNumber1
    int diceNumber2 = 2; //initializes diceNumber2
    
  
    switch (userChoice){ //initiates the switch for user choosing whether they want a random roll or to choose one
       case 1: //defines the first case in which the user types "1"
          diceNumber1 = (int)((Math.random()*6)+1); //first random dice number is generated
          diceNumber2 = (int)((Math.random()*6)+1); //second random dice number is generated
          System.out.println("You rolled: ");
          System.out.println(diceNumber1 + " " + diceNumber2); //displays what the random roll is
          System.out.println("Roll name: "); //displays roll name depending on the roll
          
          String randomRoll = (int)diceNumber1 + " " + (int)diceNumber2; //creates a string for randomRoll in the form "x x"
          switch (randomRoll){ //initiates the switch to take the dice numbers and output the name
            case "1 1":
              System.out.print("Snake Eyes"); //for roll "1 1", name outputs "Snake Eyes"
              break; //stops the code so it doesn't fall through to the next case and output the next case
            case "1 2":
              System.out.print("Ace Deuce"); //for roll "1 2", name outputs "Ace Deuce"
              break;
            case "2 1":
              System.out.print("Ace Deuce"); //for roll "2 1", name outputs "Ace Deuce"
              break;
            case "1 3":
              System.out.print("Easy Four"); //for roll "1 3", name outputs "Easy Four"
              break;
            case "3 1":
              System.out.print("Easy Four"); //for roll "3 1", name outputs "Easy Four"
              break;
            case "2 2":
              System.out.print("Hard Four"); //for roll "2 2", name outputs "Hard Four"
              break;
            case "1 4":
              System.out.print("Fever Five"); //for roll "1 4", name outputs "Fever Five"
              break;
            case "4 1":
              System.out.print("Fever Five"); //for roll "4 1", name outputs "Fever Five"
              break;
            case "2 3":
              System.out.print("Fever Five"); //for roll "2 3", name outputs "Fever Five"
              break;
            case "3 2":
              System.out.print("Fever Five"); //for roll "3 2", name outputs "Fever Five"
              break;
            case "1 5":
              System.out.print("Easy Six"); //for roll "1 5", name outputs "Easy Six"
              break;
            case "5 1":
              System.out.print("Easy Six"); //for roll "5 1", name outputs "Easy Six"
              break;
            case "2 4":
              System.out.print("Easy Six"); //for roll "2 4", name outputs "Easy Six"
              break;
            case "4 2":
              System.out.print("Easy Six"); //for roll "4 2", name outputs "Easy Six"
              break;
            case "3 3":
              System.out.print("Hard Six"); //for roll "3 3", name outputs "Hard Six"
              break;
            case "1 6":
              System.out.print("Seven Out"); //for roll "1 6", name outputs "Seven Out"
              break;
            case "6 1":
              System.out.print("Seven Out"); //for roll "6 1", name outputs "Seven Out"
              break;
            case "2 5":
              System.out.print("Seven Out"); //for roll "2 5", name outputs "Seven Out"
              break;
            case "5 2":
              System.out.print("Seven Out"); //for roll "5 2", name outputs "Seven Out"
              break;
            case "3 4":
              System.out.print("Seven Out"); //for roll "3 4", name outputs "Seven Out"
              break;
            case "4 3":
              System.out.print("Seven Out"); //for roll "4 3", name outputs "Seven Out"
              break;
            case "2 6":
              System.out.print("Easy Eight"); //for roll "2 6", name outputs "Easy Eight"
              break;
            case "6 2":
              System.out.print("Easy Eight"); //for roll "6 2", name outputs "Easy Eight"
              break;
            case "3 5":
              System.out.print("Easy Eight"); //for roll "3 5", name outputs "Easy Eight"
              break;
            case "5 3":
              System.out.print("Easy Eight"); //for roll "5 3", name outputs "Easy Eight"
              break;
            case "4 4":
              System.out.print("Hard Eight"); //for roll "4 4", name outputs "Hard Eight"
              break;
            case "3 6":
              System.out.print("Nine"); //for roll "3 6", name outputs "Nine"
              break;
            case "6 3":
              System.out.print("Nine"); //for roll "6 3", name outputs "Nine"
              break;
            case "4 5":
              System.out.print("Nine"); //for roll "4 5", name outputs "Nine"
              break;
            case "5 4":
              System.out.print("Nine"); //for roll "5 4", name outputs "Nine"
              break;
            case "4 6":
              System.out.print("Easy Ten"); //for roll "4 6", name outputs "Easy Ten"
              break;
            case "6 4":
              System.out.print("Easy Ten"); //for roll "6 4", name outputs "Easy Ten"
              break;
            case "5 5":
              System.out.print("Hard Ten"); //for roll "5 5", name outputs "Hard Ten"
              break;
            case "5 6":
              System.out.print("Yo-Leven"); //for roll "5 6", name outputs "Yo-Leven"
              break;
            case "6 5":
              System.out.print("Yo-Leven"); //for roll "6 5", name outputs "Yo-Leven"
              break;
            case "6 6":
              System.out.print("Boxcars"); //for roll "6 6", name outputs "Boxcars"
              break;  
          } //ends switch in case 1
        
          break; //stops the code so it doesn't fall through to the next case and output the next one
       
        
       case 2: //defines the second case in which the user inputs "2"
          System.out.print("Choose first integer from 1-6: "); //prompts user to pick number 1-6
          diceNumber1 = myScanner.nextInt(); //allows for user input
          System.out.print("Choose second integer from 1-6: "); //prompts user to pick number 1-6
          diceNumber2 = myScanner.nextInt(); //allows for user input
          System.out.println("Roll name: "); //displays roll name depending on the following cases
          String chooseRoll = (int)diceNumber1 + " " + (int)diceNumber2; //creates a string chooseRoll in form "x x"
          switch (chooseRoll){ //initiates the switch to take the dice numbers and output the name
            case "1 1":
              System.out.print("Snake Eyes"); //for roll "1 1", name outputs "Snake Eyes"
              break; //stops the code so it doesn't fall through to the next case and output that one
            case "1 2":
              System.out.print("Ace Deuce"); //for roll "1 2", name outputs "Ace Deuce"
              break;
            case "2 1":
              System.out.print("Ace Deuce"); //for roll "2 1", name outputs "Ace Deuce"
              break;
            case "1 3":
              System.out.print("Easy Four"); //for roll "1 3", name outputs "Easy Four"
              break;
            case "3 1":
              System.out.print("Easy Four"); //for roll "3 1", name outputs "Easy Four"
              break;
            case "2 2":
              System.out.print("Hard Four"); //for roll "2 2", name outputs "Hard Four"
              break;
            case "1 4":
              System.out.print("Fever Five"); //for roll "1 4", name outputs "Fever Five"
              break;
            case "4 1":
              System.out.print("Fever Five"); //for roll "4 1", name outputs "Fever Five"
              break;
            case "2 3":
              System.out.print("Fever Five"); //for roll "2 3", name outputs "Fever Five"
              break;
            case "3 2":
              System.out.print("Fever Five"); //for roll "3 2", name outputs "Fever Five"
              break;
            case "1 5":
              System.out.print("Easy Six"); //for roll "1 5", name outputs "Easy Six"
              break;
            case "5 1":
              System.out.print("Easy Six"); //for roll "5 1", name outputs "Easy Six"
              break;
            case "2 4":
              System.out.print("Easy Six"); //for roll "2 4", name outputs "Easy Six"
              break;
            case "4 2":
              System.out.print("Easy Six"); //for roll "4 2", name outputs "Easy Six"
              break;
            case "3 3":
              System.out.print("Hard Six"); //for roll "3 3", name outputs "Hard Six"
              break;
            case "1 6":
              System.out.print("Seven Out"); //for roll "1 6", name outputs "Seven Out"
              break;
            case "6 1":
              System.out.print("Seven Out"); //for roll "6 1", name outputs "Seven Out"
              break;
            case "2 5":
              System.out.print("Seven Out"); //for roll "2 5", name outputs "Seven Out"
              break;
            case "5 2":
              System.out.print("Seven Out"); //for roll "5 2", name outputs "Seven Out"
              break;
            case "3 4":
              System.out.print("Seven Out"); //for roll "3 4", name outputs "Seven Out"
              break;
            case "4 3":
              System.out.print("Seven Out"); //for roll "4 3", name outputs "Seven Out"
              break;
            case "2 6":
              System.out.print("Easy Eight"); //for roll "2 6", name outputs "Easy Eight"
              break;
            case "6 2":
              System.out.print("Easy Eight"); //for roll "6 2", name outputs "Easy Eight"
              break;
            case "3 5":
              System.out.print("Easy Eight"); //for roll "3 5", name outputs "Easy Eight"
              break;
            case "5 3":
              System.out.print("Easy Eight"); //for roll "5 3", name outputs "Easy Eight"
              break;
            case "4 4":
              System.out.print("Hard Eight"); //for roll "4 4", name outputs "Hard Eight"
              break;
            case "3 6":
              System.out.print("Nine"); //for roll "3 6", name outputs "Nine"
              break;
            case "6 3":
              System.out.print("Nine"); //for roll "6 3", name outputs "Nine"
              break;
            case "4 5":
              System.out.print("Nine"); //for roll "4 5", name outputs "Nine"
              break;
            case "5 4":
              System.out.print("Nine"); //for roll "5 4", name outputs "Nine"
              break;
            case "4 6":
              System.out.print("Easy Ten"); //for roll "4 6", name outputs "Easy Ten"
              break;
            case "6 4":
              System.out.print("Easy Ten"); //for roll "6 4", name outputs "Easy Ten"
              break;
            case "5 5":
              System.out.print("Hard Ten"); //for roll "5 5", name outputs "Hard Ten"
              break;
            case "5 6":
              System.out.print("Yo-Leven"); //for roll "5 6", name outputs "Yo-Leven"
              break;
            case "6 5":
              System.out.print("Yo-Leven"); //for roll "6 5", name outputs "Yo-Leven"
              break;
            case "6 6":
              System.out.print("Boxcars"); //for roll "6 6", name outputs "Boxcars"
              break;  
          } //ends switch in case 2
        
          break; //stops the code so it doesn't fall through to the next case and output the next one
       
        
       default: //default case for if the user inputs an invalid integer from 1-2
          System.out.print("Invalid input, please reset and try again"); //displays that input was invalid and prompts them to try again
          break;
      } //end of switch statement
    
  } //end of main method
} //end of class
