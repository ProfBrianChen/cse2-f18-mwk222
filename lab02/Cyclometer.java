//Matt Knowles
//mwk222@lehigh.edu
//CSE 002-110 lab02 - Arithmetic Calculations
public class Cyclometer{
  //Main is required for java programs
  public static void main(String[] args){
    //Program will store numerical values and perform calculations
    //Determine bicycle speed, distance, and time
    
    //input data
    int secsTrip1 = 480; //Trip 1 took 480 seconds to complete
    int secsTrip2 = 3220; //Trip 2 took 3220 seconds to complete
    int countsTrip1 = 1591; //Trip 1 had 1591 rotations
    int countsTrip2 = 9037; //Trip 2 had 9037 rotations
      
    //constants and output data
    double wheelDiameter = 27.0; //Diameter of wheel is exactly 27.0
    double PI = 3.14159; //PI will be used up to 5 decimal places
    int feetPerMile = 5280; //There are 5280 feet in a mile
    int inchesPerFoot = 12; //There are 12 inches in a foot
    int secondsPerMinute = 60; //There are 60 seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //All distances will be doubles
   
    //Print stored input data with constants to find time and counts
    System.out.println("Trip 1 took "+ (secsTrip1 / secondsPerMinute) + 
           " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took "+ (secsTrip2 / secondsPerMinute) + 
           " minutes and had " + countsTrip2 + " counts.");
    
    //Run calculations
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //Distance in inchesPerFoot
    //Rotation of the wheel causes bicycle to travel (diameter * PI) in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Distance converted to miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; //Same as line 31 comment
    distanceTrip2 /= inchesPerFoot * feetPerMile; //Same as line 33 comment
    totalDistance = distanceTrip1 + distanceTrip2; //Sum of distance from Trip1 and Trip2 yields totalDistance
    
    //Print stored output data to find distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
    System.out.println("Trip 2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance was "+totalDistance+" miles.");
    
  } //end of main
} //end of class