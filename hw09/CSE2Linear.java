//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw09 - CSE2Linear
//11/20/18

import java.util.*; //imports all the java.util tools such as scanner, math, and arrays

public class CSE2Linear{ 
  //main method
  public static void main(String[] args){ 
    Scanner myScanner = new Scanner(System.in); //imports a new scanner
    int[] finalGrade = new int[15]; //declares and sets the index value for a new array
    
    System.out.println("Please enter 15 integers from 0-100 as final grades in CSE2, each one greater than or equal to the last"); //prompts user to enter 15 integers as final grades
    
    //for loop to catch the errors
    for(int i = 0; i < 15; i++){
      System.out.print("Value: ");
      boolean correctInt = false;  
      //while loop continues as long as a non-integer is entered
      while(!correctInt){ 
        correctInt = myScanner.hasNextInt(); //makes sure an int is entered
        if (correctInt == true){
          finalGrade[i] = myScanner.nextInt(); //if user does enter an int, scanner lets user enter value
          
          while (!(finalGrade[i] >= 0 && finalGrade[i] <= 100)){ //makes sure grade entered is between 0-100
             System.out.println("Integer must be between 0-100");
             System.out.print("Value: ");
             finalGrade[i] = myScanner.nextInt(); //allows user to try again
             if (finalGrade[i] >= 0 && finalGrade[i] <= 100){ //when user enters the correct value the loop breaks
                 break;
               }
          } 
           
          while(i != 0){
            if(finalGrade[i] < finalGrade[i-1]){ //makes sure each grade entered is greater than or equal to the last one entered
              System.out.println("Integer must be greater than or equal to the last one");
              System.out.print("Value: ");
              finalGrade[i] = myScanner.nextInt(); //allows user to try again
            }
            else { //user can keep entering integers when they're doing it correctly
              break; 
            }
          }
        }
        else { //when user doesn't enter an integer, they are allowed to try again
          System.out.println("Invalid input, please enter an integer");
          System.out.print("Value: ");
          myScanner.next(); 
        }
      }
    }
    
    System.out.println(Arrays.toString(finalGrade)); //prints out the first list
    
    System.out.print("Enter a grade to be searched for: ");
    int index = myScanner.nextInt(); //user enters value to search for
    int keyIndex = binarySearch(finalGrade, index); //keyIndex is the output of the binarySearch method
    if (keyIndex == -1){ //when the method returns -1, the value searched for was not in the list
      System.out.println(index + " was not found");
    }
    
    System.out.println("Scrambled:");
    scrambled(finalGrade); //scrambles the original list
    
    System.out.print("Enter a grade to be searched for: ");
    index = myScanner.nextInt(); //user enters value to search for
    keyIndex = linearSearch(finalGrade, index); //keyIndex is now the output of the linearSearch method
    if (keyIndex == -1){ //when the method returns -1, the value searched for was not in the list
      System.out.println(index + " was not found");
    }
    
  }
  
  //binarySearch method
  public static int binarySearch(int[] finalGrade, int index) {
    int steps = 0; //steps are incremented in each loop to indicate how many iterations it took to find value
    int low = 0; //low index will always start at 0
    int high = finalGrade.length - 1; //high index starts at the end of the list
    while(high >= low) { //continues to loop when the high index is greater than the low index
     int mid = (low + high)/2; //middle is the average of high and low index
     if (finalGrade[mid] < index) { //when the middle is lower than the search value, low becomes the index above the middle
       low = mid + 1;
       steps++;
     }
     else if (finalGrade[mid] > index) { //when the middle is higher than the search value, high becomes the index below the middle
       high = mid - 1;
       steps++;
     }
     else { //middle equals the search value and the print statement says how many steps it took
       steps++;
       System.out.println(index + " was found in " + steps + " steps");
       return mid;
     }
    }
  return -1; //when the value is not found, the method returns -1
  
  }
  
  //scrambled method
  public static void scrambled(int[] finalGrade){
    int j = 0;
    for (int i = 0; i < 200; i++){ //loops 200 times to shuffle properly
      j = (int)((Math.random() * 14) + 1); //j is a random value from 0-14
      int temp = finalGrade[0]; //temp value to hold the initial index
      finalGrade[0] = finalGrade[j]; //value at index 0 is now the value at index j
      finalGrade[j] = temp; //value at index j equals value it was switched with
      j++; //moves index j up one
    } 
    System.out.println(Arrays.toString(finalGrade)); //prints out the scrambled list
  
  }
  
  //linearSearch method
  public static int linearSearch(int[] finalGrade, int index){
    for (int i = 0; i < finalGrade.length; i++){ //runs through the whole list
      if (index == finalGrade[i]){ //when the search value equals finalGrade[i], print statement signals how many steps
        System.out.println(index + " was found in " + (i + 1) + " steps");
        return i; //returns the search value
      }
    }
    return -1; //when value is not found, -1 is returned
    
  }
  
} //end of class