//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw09 - RemoveElements
//11/25/18

import java.util.Scanner; //imports the scanner tool
import java.util.Random; //imports the scanner tool

public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in); //creates a new scanner called "scan"
    
    //declares 3 arrays
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];

    int index,target; //declares the index and target the user will enter
    
    String answer="";
    do {
      System.out.println("Random input 10 ints [0-9]");
      num = randomInput(); //creates an array of random integers with length 10
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out); //displays the first array

      System.out.print("Enter the index to be deleted ");
      index = scan.nextInt(); //user can enter an index to be removed
      while (!(index >= 0 && index <= 9)){  //makes sure user enters int between 0-9
             System.out.println("Integer must be between 0-9");
             System.out.print("Enter the index to be deleted ");
             index = scan.nextInt(); //allows user to try again
             if (index >= 0 && index <= 9){ //when user enters the correct value the loop breaks
                 break;
               }
      }
      newArray1 = delete(num,index); //creates a new array with the removed index
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1); //displays array with removed index

      System.out.print("Enter the target value ");
      target = scan.nextInt(); //user can enter a value to be removed
      while (!(index >= 0 && index <= 9)){ //makes sure user enters int between 0-9
             System.out.println("Integer must be between 0-9");
             System.out.print("Enter the value to be deleted ");
             index = scan.nextInt(); //allows user to try again
             if (index >= 0 && index <= 9){ //when user enters the correct value the loop breaks
                 break;
               }
      }
      newArray2 = remove(num,target); //creates a new array with the removed value
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2); //displays array with removed value
 
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next(); //allows user to choose if they want to start over
    } while (answer.equals("Y") || answer.equals("y"));
  }

  public static String listArray(int num[]){
    String out = "{";
    for (int j = 0; j < num.length; j++){ //creates how the arrays will look ( {a, b, c} )
      if (j > 0){
        out += ", ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  }
  
  public static int[] randomInput(){
    int[] num = new int[10]; //creates a list of length 10
    for (int i = 0; i < num.length; i++){ //randomizes each index between 0-9
      num[i] = (int)((Math.random() * 9) + 1);
    }
    return num; //returns the created list
  }
  
  public static int[] delete(int list[], int pos){
    int[] newList = new int[9]; //creates new list of length 9
    
    for (int i = 0; i < pos; i++){ //copies original list into new list up to the index to be removed
      newList[i] = list[i];
    }
    
    for (int i = 0; i < 9; i++){ //replaces the chosen index value with the next value 
      if (i == pos){
        newList[i] = list[i + 1];
      }
    }
    
    for (int i = pos + 1; i < 9; i++){ //starts after the chosen index and copies the remaining values of original list to new list
      newList[i] = list[i + 1];
    }
    
    return newList; //returns the new list with removed index
  
  }
  
  public static int[] remove(int list[], int target){
    int count = 0;
    
    for (int i = 0; i < list.length; i++){ //counts how many target values are in list
      if (list[i] == target){
        count++;
      }
    }
    
    int[] newList = new int[10 - count]; //creates new list with a length of the difference between 10 and the number of values
    
    for (int i = 0; i < 10 - count; i++){ //copies the original list into a new list with new length
      newList[i] = list[i];
    }
    
    int j = 0;
    for (int i = 0; i < 10 - count; i++){ //when the index of the new list isn't the target value, the next value is entered into the new list
      if (newList[i] != target){
        newList[j] = list[i];
        j++;
      }
    }
    
    return newList; //returns the new list with values removed
    
  }
  
}
