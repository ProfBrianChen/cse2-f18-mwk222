//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 hw06 - EncryptedX
//10/19/18

import java.util.Scanner; //imports the scanner

public class EncryptedX{ //defines class EncryptedX
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //defines and initializes "myScanner"
    
    System.out.println("Enter an integer from 1-100: "); //prompts the user to enter integer from 1-100
    int squareSize = 0; //defines squareSize
   
    
    boolean correctInt = false; //defines the boolean correctInt
    while (!correctInt){ //begins a while loop with the notion that if an integer is not entered, the user must try again
      correctInt = myScanner.hasNextInt(); //coorectInt takes an input
      
      if (correctInt == true){ //when an integer is entered, the scanner will read it
        while (!(squareSize >= 1 && squareSize <= 100)){ //creates a while loop that runs the interior when the integer isn't 1-100
          System.out.println("Integer must be from 1-100: "); //displays error message, and prompts user to enter integer 1-100
          squareSize = myScanner.nextInt(); //allows user to enter an integer again
        }
      }
      else { //runs when a user does not input an integer
        System.out.println("Invalid input, please enter an integer: "); //if the user does not enter an integer, they are prompted to try again
        myScanner.next(); //removes unwanted input
        }
    }
    
    
    int row = 0; //defines row
    int j = 0; //defines variable for loops
    
    for (row = 0; row <= squareSize; row++){ //for loop that defines the row number
      for (j = 0; j <= squareSize; j++){ //for loop that prints the spaces and stars
        if (j == row){ //when j equals the row number, a space is printed
          System.out.print(" ");
          continue; //other statement can run
        }
        if (j == squareSize - row){ //prints a space from the far side of the grid and decreases by 1 per row
          System.out.print(" ");
        }
        else { //j prints out a star for all other positions
        System.out.print("*");
        }
      }
      System.out.println(); //skips to the next line after a row is printed
    }
    
  } //end of main method
} //end of class