//Matt Knowles
//mwk222@lehigh.edu
//CSE 002-110 
//hw03 - Convert
import java.util.Scanner; //this allows the Scanner class to be used without getting error messages

public class Convert{
  
  public static void main(String[] args){
    //program will take input from user and convert to rain in cubic miles
    
    Scanner myScanner = new Scanner (System.in); //must declare the scanner before using it and having the program accept input
    
    System.out.print("Enter the affected area in acres rounded to hundreths place: "); //prompts user to input acres of land affected
    double numAcresPrecipitation = myScanner.nextDouble(); //number of acres affected by hurricane precipitation
    
    System.out.print("Enter the rainfall in the affected area as a whole number: "); //prompts user to input amount of rainfall
    double numInchesRain = myScanner.nextDouble(); //number of inches of rain in affected area
 
    double cubicMiles; //defines quanitiy of rain in cubic miles
    //.0015625 square miles in an acre
    //63360 inches in a mile
   
    cubicMiles = (numAcresPrecipitation * numInchesRain * .0015625) / 63360;
    //formula to convert acres and inches of rainfall to cubic miles using acres to square miles and inches to miles
                  
    System.out.print("The quantity of rain is " + cubicMiles + " cubic miles"); //tells user what the cubic miles will be
    
  } //end of main method 
} //end of class