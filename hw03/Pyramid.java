//Matt Knowles
//mwk222@lehigh.edu
//CSE 002-110 
//hw03 - Pyramid
import java.util.Scanner; //this allows the Scanner class to be used without getting error messages

public class Pyramid{
  
  public static void main(String[] args){
    //program will take dimensions of a pyramid and output the volume of that pyramid
    
    Scanner myScanner = new Scanner (System.in); //must declare the scanner before using it and having the program accept input

    System.out.print("Enter the square side of the pyramid: "); //prompts user to input side length of pyramid
    double squareSidePyramid = myScanner.nextDouble(); //defines side of pyramid as variable and allows user to input value
    
    System.out.print("Enter the height of the pyramid: "); //prompts user to input height of pyramid
    double heightPyramid = myScanner.nextDouble(); //defines height of pyramid as variable and allows user to input value
    
    double volumePyramid; //defines volume inside of pyramid
    
    volumePyramid = (squareSidePyramid * squareSidePyramid * heightPyramid) / 3; 
    //formula to find volume inside pyramid
    
    System.out.print("The volume inside the pyramid is: " + volumePyramid); //tell the user what the volume inside the pyramid is
  
  } //end of main method 
} //end of class