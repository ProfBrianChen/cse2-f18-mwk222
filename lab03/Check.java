//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab03 - Check
//Program will calculate how to split a bill evenly between people at a restaurant
import java.util.Scanner; //this allows the Scanner class to be used without getting error messages

public class Check{ //main method required for Java programs
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner (System.in); //must declare the scanner before using it and having the program accept input
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //user is prompted to input the cost of the bill
    double checkCost = myScanner.nextDouble(); //defines the variable checkCost and allows for user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //user is prompted to input tip percentage
    double tipPercent = myScanner.nextDouble(); //defines the variable tipPercent and allows for user input
    tipPercent /= 100; //converts percentage into decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //user is prompted to input number of people at restaurant
    int numPeople = myScanner.nextInt(); //defines the variable numPeople and allows for user input
    
    double totalCost; //defines the variable total cost of the meal
    double costPerPerson; //defines the variable how much the cost is per person 
    int dollars, dimes, pennies; //defines all the money values 
    totalCost = checkCost * (1 + tipPercent); //finds cost after tip is added
    costPerPerson = totalCost / numPeople; //finds cost per person
    dollars = (int)costPerPerson; //finds the dollar values as an integer
    dimes = (int)(costPerPerson * 10) % 10; //finds the values of the tenths place as an integer
    pennies = (int)(costPerPerson * 100) % 10; //finds the values of the hundreths place as an integer
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies + '.'); //displays how much each person owes
    
  } //end of main method
} //end of class
