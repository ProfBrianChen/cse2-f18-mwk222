//Matt Knowles
//mwk222@lehigh.edu
//CSE002-110 lab05 - readingInputs
//program will ask user to enter information relating to a course they are taking
//it will also give an error message when they enter incorrect inputs and prompt them to input a correct object

import java.util.Scanner; //allows user to write an input

public class readingInputs{ //the class is readingInputs
  public static void main(String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); //defines a scanner that can be used later
    
    //course number
    System.out.println("Enter your course number:"); //prompts user to input course number
    
    boolean correctInt = false; //defines the boolean correctInt
    while(!correctInt){ //begins a while loop with the notion that if an integer is not entered, the user must try again
      correctInt = myScanner.hasNextInt(); //coorectInt takes an input
      
      if (correctInt == true){ //when an integer is entered, the scanner will read it
        int courseNum = myScanner.nextInt(); //defines course number as an integer and allows user to input an integer for course number
        System.out.println("Course number: " + courseNum); //displays course number
        } //ends if statement
      else { 
        System.out.println("Invalid input, please enter an integer:"); //if the user does not enter an integer, they are prompted to try again
        myScanner.next(); //removes unwanted input
        } //ends else statement
      
      } //ends while loop
      
    //department name
    System.out.println("Enter the department name:"); //prompts user to input course number
    
    boolean correctString = false; //defines the boolean correctString
    while(!correctString){ //begins a while loop with the notion that if a string is not entered, the user must try again
      correctString = myScanner.hasNext(); //correctString takes an input
      
      if (correctString == true){ //when a string is entered, the scanner will read it
        String depName = myScanner.next(); //defines depName as a string and allows input of a string
        System.out.println("Department name: " + depName); //displays department name
      } //ends if statement 
      else {
        System.out.println("Invalid input, please enter a name:"); //if user does not enter string, they are asked to try again
        myScanner.next(); //removes unwanted input
      } //ends else statement
      
    } //ends while loop
    
    //how often it meets
    System.out.println("Enter how many times per week does the course meet:"); //prompts user to input how often class meets
    
    correctInt = false; //defines correctInt for while loop
    while(!correctInt){ //begins while loop with notion that if string isn't entered, user must try again
      correctInt = myScanner.hasNextInt(); //correctInt takes an input
      
      if (correctInt == true){ //when an integer is entered, the scanner will read it
        int meetPerWeek = myScanner.nextInt(); //defines meetPerWeek as an integer and allows input
        System.out.println("Meeting times per week: " + meetPerWeek); //displays meeting times per week
        } //ends if statement 
      else {
        System.out.println("Invalid input, please enter an integer:"); //if user does not enter integer, they are asked to try again
        myScanner.next(); //removes unwanted input
        } //ends else statement
      
      } //ends while loop
    
    //time class starts
    System.out.println("Enter the time your class starts in military time: (XXXX)"); //prompts user to enter class start time
    
    correctInt = false; //defines correctInt for while loop
    while(!correctInt){ //begins while loop with notion that if integer isn't entered, user must try again
      correctInt = myScanner.hasNextInt(); //correctInt takes an input
      
      if (correctInt == true){ //when integer is entered, scanner reads it
        int startTime = myScanner.nextInt(); //defines startTime as an integer and allows input
        System.out.println("Start time: " + startTime); //displays meeting times per week
        } //ends if statement
      else {
        System.out.println("Invalid input, please enter an integer: (XXXX)"); //if user does not enter integer, they are asked to try again
        myScanner.next(); //removes unwanted input
        } //ends else statement
      
      } //ends while loop
    
    //instructor name
    System.out.println("Enter your instructor's name: (last name)"); //prompts user to enter instructor's name
    
    correctString = false; //defines correctString for while loop
    while(!correctString){ //begins while loop with notion that if string isn't entered, user must try again
      correctString = myScanner.hasNext(); //correctString takes an input
      
      if (correctString == true){ //when string is entered, scanner reads it
        String instructorName = myScanner.next(); //defines instructorName as a string and allows input
        System.out.println("Instructor's name: " + instructorName); //displays instructor's name
      } //ends if statement
      else {
        System.out.println("Invalid input, please enter a name:"); //if user does not enter string, they are asked to try again
        myScanner.next(); //removes unwanted input
      } //ends else statement
      
    } //ends while loop
    
    //number of students
    System.out.println("Enter the number of students in your class:"); //prompts user to enter number of students in class
    
    correctInt = false; //defines correctInt for while loop
    while(!correctInt){ //begins while loop with notion that if integer isn't entered, user must try again
      correctInt = myScanner.hasNextInt(); //correctInt takes an input
      
      if (correctInt == true){ //when integer is entered, scanner reads it
        int numStudents = myScanner.nextInt(); //defines numStudents as an integer and allows input
        System.out.println("Number of students: " + numStudents); //displays number of students
        } //ends if statement
      else {
        System.out.println("Invalid input, please enter an integer:"); //if user does not enter integer, they are asked to try again
        myScanner.next(); //removes unwanted input
        } //ends else statement
      
      } //ends while loop
  } //ends main method
} //ends class